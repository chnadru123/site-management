import frappe, erpnext

def on_submit(self,method):
	
	update_cost_in_project_custom(self)

def update_cost_in_project_custom(self):
    # frappe.msgprint("here")
    if self.project and self.stock_entry_type == "Material Return":
        # frappe.msgprint("if")
        amount = frappe.db.sql(""" select ifnull(sum(sed.amount), 0)
            from
                `tabStock Entry` se, `tabStock Entry Detail` sed
            where
                se.docstatus = 1 and se.project = %s and sed.parent = se.name
                and (sed.t_warehouse is null or sed.t_warehouse = '')""", self.project, as_list=1)

        ret_amount = frappe.db.sql(""" select ifnull(sum(sed.amount), 0)
            from
                `tabStock Entry` se, `tabStock Entry Detail` sed
            where
                se.docstatus = 1 and se.project = %s and sed.parent = se.name
                and (sed.s_warehouse is null or sed.s_warehouse = '') and se.stock_entry_type = "Material Return" """, self.project, as_list=1)
        # frappe.msgprint(str(ret_amount))
        return_amount = ret_amount[0][0] if ret_amount else 0

        amount = amount[0][0] if amount else 0
        # frappe.msgprint(str(return_amount))
        # frappe.msgprint(str(amount))
        frappe.db.set_value('Project', self.project, 'total_returned_stock', return_amount)
        frappe.db.set_value('Project', self.project, 'actual_consumed_stock', amount - return_amount)